package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/kardianos/service"
	"gopkg.in/natefinch/lumberjack.v2"
	"gopkg.in/robfig/cron.v2"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var logFile *log.Logger
var logger service.Logger
var cronProcess = cron.New()
var tomcatUrlPtr *string
var urlTimeoutPtr *int
var javaProcessNamePtr *string
var javaProcessUserPtr *string
var tomcatScheduledTaskPtr *string
var scheduledTaskSucessMessagePtr *string
var javaProcessCheckDelayPtr *int
var cronSchedulingPtr *string
var checkRestartTimeoutPtr *int
var checkRestartAttemptDelayPtr *int
var isProcessing bool

type configuration struct {
	TomcatUrl                  string
	UrlTimeout                 int
	JavaProcessName            string
	JavaProcessUser            string
	TomcatScheduledTask        string
	ScheduledTaskSucessMessage string
	JavaProcessCheckDelay      int
	CronScheduling             string
	CheckRestartTimeout        int
	CheckRestartAttemptDelay   int
}
type program struct {
}

func init() {
	exeDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	if _, err := os.Stat(exeDir + "/logs"); os.IsNotExist(err) {
		if err := os.Mkdir(exeDir+"/logs", 0666); err != nil {
			log.Fatal(err)
		}
	}

	file, err := os.OpenFile(exeDir+"./logs/logfile.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}

	logFile = log.New(file, "", log.Ldate|log.Ltime)
	logFile.SetOutput(&lumberjack.Logger{
		Filename: exeDir + "./logs/logfile.log",
		MaxSize:  10,
		MaxAge:   30,
	})

	_, err = createLockFile(exeDir + "/lockfile")
	if err != nil {
		logFile.Println("Executable already running")
		log.Fatal("Executable already running")
	}

	configuration, err := loadConfigurationFile(exeDir + "/config.json")
	if err != nil {
		logFile.Println("Unable to parse configuration file")
		logFile.Println(err.Error())
		os.Exit(1)
	}

	tomcatUrlPtr = &configuration.TomcatUrl
	urlTimeoutPtr = &configuration.UrlTimeout
	javaProcessNamePtr = &configuration.JavaProcessName
	javaProcessUserPtr = &configuration.JavaProcessUser
	tomcatScheduledTaskPtr = &configuration.TomcatScheduledTask
	scheduledTaskSucessMessagePtr = &configuration.ScheduledTaskSucessMessage
	javaProcessCheckDelayPtr = &configuration.JavaProcessCheckDelay
	cronSchedulingPtr = &configuration.CronScheduling
	checkRestartTimeoutPtr = &configuration.CheckRestartTimeout
	checkRestartAttemptDelayPtr = &configuration.CheckRestartAttemptDelay

}

func main() {

	svcConfig := &service.Config{
		Name:        "GMAO API Tomcat supervisor",
		DisplayName: "GMAO API Tomcat supervisor",
		Description: "API Tomcat Monitoring Service ",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		logFile.Println(err)
		log.Fatal(err)
	}

	if len(os.Args) > 1 {
		err = service.Control(s, os.Args[1])
		if err != nil {
			logFile.Println(err)
			log.Fatal(err)
		}
		return
	}

	logger, err = s.Logger(nil)
	if err != nil {
		logFile.Println(err)
		log.Fatal(err)
	}

	err = s.Run()
	if err != nil {
		logFile.Println(err)
		log.Fatal(err)
	}
}

func (p *program) Start(s service.Service) error {
	go p.run()
	return nil
}

func (p *program) Stop(s service.Service) error {
	logFile.Println("Stopping service GMAO API Tomcat supervisor")
	return nil
}

func (p *program) run() error {
	logFile.Println("Running GMAO API Tomcat supervisor")
	cronProcess.AddFunc(*cronSchedulingPtr, func() { processTomcatCheck() })
	isProcessing = false
	cronProcess.Start()
	return nil
}

func createLockFile(filename string) (*os.File, error) {
	if _, err := os.Stat(filename); err == nil {
		err = os.Remove(filename)
		if err != nil {
			return nil, err
		}

	}
	return os.OpenFile(filename, os.O_CREATE|os.O_EXCL|os.O_RDWR, 0666)
}

func loadConfigurationFile(file string) (*configuration, error) {
	var config = new(configuration)

	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		return config, err
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		return config, err
	}
	return config, err
}

func checkUrlResponseStatusCode(timeout time.Duration, url string) (result bool, error error) {
	client := http.Client{
		Timeout: timeout,
	}

	resp, err := client.Get(url)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		return true, nil
	} 
	return false, nil	
}

func loopCheckURL(timeout time.Duration, url string) bool {
	loopTimeout := time.After(time.Duration(*checkRestartTimeoutPtr) * time.Second)
	loopTick := time.Tick(time.Duration(*checkRestartAttemptDelayPtr) * time.Second)
	for {
		select {
		case <-loopTimeout:
			return false

		case <-loopTick:
			stopLoop, _ := checkUrlResponseStatusCode(timeout, url)
			if stopLoop {
				return true
			}
		}
	}
}

func findProcessByNameForUser(processName string, userName string) (bool, error) {
	cmd := exec.Command("tasklist", "/FI", "IMAGENAME eq "+processName, "/FI", "username eq "+userName, "/FO", "csv", "/nh")
	out, err := cmd.Output()
	if err != nil {
		return false, err
	}

	if bytes.Contains(out, []byte(processName)) {
		return true, nil
	}
	return false, nil
}

func findAndKillProcessesByNameForUser(processName string, userName string) error {
	cmd := exec.Command("tasklist", "/FI", "IMAGENAME eq "+processName, "/FI", "username eq "+userName, "/FO", "csv", "/nh")
	out, err := cmd.Output()
	if err != nil {
		return err
	}
	if bytes.Contains(out, []byte(processName)) {
		outStr := string(out[:])
		outStr = strings.TrimRight(outStr, "\n")
		outStrLines := strings.Split(outStr, "\n")
		numLines := len(outStrLines)
		for i := 0; i < numLines; i++ {
			lineFields := strings.Split(strings.Replace(outStrLines[i], "\"", "", -1), ",")
			cmd := exec.Command("taskkill", "/PID", lineFields[1], "/F")
			err := cmd.Run()
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func runScheduledTaskByTaskName(taskName string) (bool, error) {
	cmd := exec.Command("schtasks", "/Run", "/TN", taskName)
	out, err := cmd.Output()
	if err != nil {
		return false, err
	}
	if bytes.Contains(out, []byte(taskName)) && bytes.Contains(out, []byte(*scheduledTaskSucessMessagePtr)) {
		return true, nil
	} 
	return false, errors.New(string(out[:]))
}

func processTomcatCheck() bool {
	if isProcessing == true {
		return false
	}

	isProcessing = true

	logFile.Println("Begin of Tomcat server check")

	result, err := checkUrlResponseStatusCode(time.Duration(time.Duration(*urlTimeoutPtr)*time.Second), *tomcatUrlPtr)
	if err != nil {
		logFile.Println("--- " + err.Error())
	}

	if !result {
		logFile.Println("--- Restarting Tomcat server...")
		err := findAndKillProcessesByNameForUser(*javaProcessNamePtr, *javaProcessUserPtr)
		if err != nil {
			logFile.Println("--- Error while killing " + *javaProcessNamePtr + " for user " + *javaProcessUserPtr)
			logFile.Println("--- " + err.Error())
			logFile.Println("End of Tomcat server check")
			isProcessing = false
			return false
		}

		success, err := runScheduledTaskByTaskName(*tomcatScheduledTaskPtr)
		if err != nil {
			logFile.Println("--- Error while starting task " + *tomcatScheduledTaskPtr)
			logFile.Println("--- " + err.Error())
			logFile.Println("End of Tomcat server check")
			isProcessing = false
			return false
		}
		if success {
			time.Sleep(time.Duration(*javaProcessCheckDelayPtr) * time.Second)
			process, err := findProcessByNameForUser(*javaProcessNamePtr, *javaProcessUserPtr)
			if err != nil {
				logFile.Println("--- Error while searching " + *javaProcessNamePtr + " for user " + *javaProcessUserPtr)
				logFile.Println("--- " + err.Error())
				logFile.Println("End of Tomcat server check")
				isProcessing = false
				return false
			}
			if !process {
				logFile.Println("--- " + *javaProcessNamePtr + " not found for user " + *javaProcessUserPtr)
				logFile.Println("End of Tomcat server check")
				isProcessing = false
				return true
			}

			logFile.Println("--- Checking " + *tomcatUrlPtr + " during " + strconv.Itoa(*checkRestartTimeoutPtr) + " seconds to confirm restart. Please wait... ")
			if loopCheckURL(time.Duration(*urlTimeoutPtr)*time.Second, *tomcatUrlPtr) {
				logFile.Println("--- Tomcat successfully restarted")
				logFile.Println("End of Tomcat server check")
			} else {
				logFile.Println("--- Tomcat process found but " + *tomcatUrlPtr + " don't respond after " + strconv.Itoa(*checkRestartTimeoutPtr) + " seconds")
				logFile.Println("End of Tomcat server check")
			}
			isProcessing = false
			return true
		}
	} else {
		logFile.Println("End of Tomcat server check")
	}
	isProcessing = false
	return false
}
